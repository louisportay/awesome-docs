# AWESOME-DOCS

Because keeping tabs of articles and useful repos is part of the job !

## Subdirectories:
 * [Binaries](binaries/README.md)

## General resources:
 * [Awesome-Go](https://awesome-repos.ecp.plus/go.html#Natural%20Language%20Processing): an Awesome Go repository but with stars. Even if Stars are not always a good indicator, at least there is one.
 * [Source Graph](https://sourcegraph.com/github.com/olivere/elastic@3e93e18/-/blob/search_queries_query_string.go#L33:2): `Universal Code Search` - Search with ref and backref in a repo (public for free version, but really strong.
