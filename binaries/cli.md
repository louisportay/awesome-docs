# CLI Binaries

 * [Glow](https://github.com/charmbracelet/glow): Beautiful reader for Markdowns
 * [RigGrep](https://github.com/BurntSushi/ripgrep): Grep but... Better, Faster, Stronger... ?
 * [Bat](https://github.com/sharkdp/bat): Really strong and fast and customizable pager.
 * [FD](https://github.com/sharkdp/fd): Another `sharkdp` binary. Find but once again, better, faster, stronger.
